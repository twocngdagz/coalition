var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass('app.scss', 'resources/assets/css/app.css');
    mix.copy('node_modules/jquery/dist/jquery.min.js', 'resources/assets/js/jquery/jquery.min.js');
    mix.copy('node_modules/datatables/media/js/jquery.dataTables.min.js', 'resources/assets/js/datatables/jquery.dataTables.min.js');
    mix.copy('node_modules/datatables/media/images', 'public/images');
    mix.copy('node_modules/datatables/media/css/jquery.dataTables.min.css', 'resources/assets/css/datatables/jquery.dataTables.min.css');
    mix.copy('resources/assets/fonts/bootstrap/**', 'public/fonts/bootstrap').copy('resources/assets/js/select2/i18n', 'public/js/i18n')
    	.scripts([
    		'jquery/jquery.min.js', 
            'bootstrap.min.js', 
            'datatables/jquery.dataTables.min.js', 
            'function.js'
		], 'public/js/app.js');
	mix.styles([
		'app.css', 
        'datatables/jquery.dataTables.min.css', 
	], 'public/css/main.css');
});
